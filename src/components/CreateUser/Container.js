import React, { Component } from 'react'
import fire from '../Firebase/firebase'
import Presentation from './Presentation'
import {withRouter} from 'react-router-dom'
import Swal from 'sweetalert2'
import $ from 'jquery'
import axios from 'axios'

class Container extends Component {
    state={
        placeHtml:null,
        userMail:""
    }
   

    handleClick=(email)=>{
        // var userMail=this.state.userMail
        var userMail=email
        let pass=Math.floor((Math.random()*1000000));
        fire.firestore().collection("sendEmailVerify").doc(userMail).set({
            status:false,
            Password:pass
        })

        fire.firestore().collection("Users").doc(userMail).set({
            status:"Inactive",
            useremail:userMail
        })
        // axios.post("http://localhost:5001/flair-d7b59/us-central1/newUserEmail",{
        //     useremail:userMail,
        //     time : new Date()
        // })
        // .then(res => {
        //     console.log(res.data)
        // }).catch(err => {
        //     console.log(err)
        // })

        axios.post("https://us-central1-flair-d7b59.cloudfunctions.net/sendEmail",{
           email:userMail,
           pass:pass
        })
        .then(res => {
            console.log(res)
            Swal.fire({
                icon:"success",
                toast:true,
                title: userMail+" has been invited successfully",
                position: 'top-end',
                showConfirmButton: false,
                
                showClass: {
                  popup: ''
                },
                timer: 2500
              })
        }).catch(err => {
            console.log(err)
        })
       


    }

    handleChange=(e)=>{
        console.log(e.target.value);
        const userMail=e.target.value;
        this.setState({userMail:userMail})
   
    }
    componentDidMount(){
        console.log(this.props)
        fire.firestore().collection('Template').doc('HtmlTemp').get().then((snap)=>{
            // $("#placeit").html("<div>"+snap.data().content+"<div>")
            this.setState({
                placeHtml:snap.data().content
            })
            // $("#placeHtml").html(snap.data().content);
          })
          fire.auth().onAuthStateChanged((user)=>{
            if(user){
            
            }
            else{
                this.props.history.push("/login")
            }
          })
    }

    render() {

       

        return (
            <div className="mt-5">
                <Presentation adminRole={this.props.adminRole} placeHtml={this.state.placeHtml} handleChange={this.handleChange} handleClick={this.handleClick}/>
            </div>
        )
    }
}

export default withRouter(Container)
