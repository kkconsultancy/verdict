import React, { Component } from "react";
import axios from 'axios'
import Presentation from './Presentation'
import fire from '../Firebase/firebase'
class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      historyData: []
    };
  }
  componentDidMount() {
      axios.get(`http://localhost:5001/flair-d7b59/us-central1/api/history`)
      .then(data=>{
        this.setState({historyData:data.data})
        console.log(data.data)
      })
      .catch(err=>{
        console.log(err)
      })
  }
  render() {
    let tabularData=this.state.historyData.map(history=>{
      console.log(history.createdAt)
        return {
          ActionBy:history.ActionBy,
          createdAt:Date(history.createdAt._seconds),
          subject:history.subject
        }
    })
    const columns = [
      {
        Header: "User",
        accessor: "ActionBy",
        style: {
          textAlign: "center"
        },
      },
      {
        Header: "TimeStamp",
        accessor: "createdAt",
        sortable: false,
        filterable: false
      },
      {
        Header: "History",
        accessor: "subject",
        style: {
          textAlign: "center"
        },
      },
      
    ];
    return (
      <Presentation
      columns={columns}
      tabularData={tabularData}
      />
    );
  }
}
export default Container;
