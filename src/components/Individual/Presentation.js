import React from 'react'
import {Table,CustomInput,Spinner,Input} from 'reactstrap'
import $ from 'jquery'
// import 
function Presentation(props) {
    const {editWorkAuth,editWorkAuthDone,addWorkAuth,deleteWorkAuth,workAuth,empHistory,addEmpHistory,editEmpFun,editEmpDone,deleteEmpRow,deleteEmerRow,addEmergency,emergencyPlace,editEmergencyDone,editEmergency,editEmergencyFun,individual,adminRole,loadButton,uid,suspendedUser,loadEnableBtn,status,SuspendUser,EnableUser,Role,userRole,promote,demote,imageURL,userstatus,email,workauth,personal,veridicId,mailingaddress,emergencycontact,employementhistory}=props;
    console.log(emergencyPlace)
   console.log(empHistory)
    const EmerUI=emergencyPlace.map(item=>{
        return(
            <tr id={"emerRow"+item.id}>
            <td><span id={"emerRowVal1"+item.id}>{item.name==""?<span className="text-danger">Enter name</span>:item.name}</span></td>
            <td><span id={"emerRowVal2"+item.id}>{item.phone==""?<span className="text-danger">Enter phone</span>:item.phone}</span></td>
            <td><span id={"emerRowVal3"+item.id}>{item.emailid==""?<span className="text-danger">Enter emailid</span>:item.emailid}</span></td>
            <td id={"emerRowConsole"+item.id}><button className="ver-btn" id={item.id} onClick={editEmergencyFun}>Edit</button>&nbsp;&nbsp;<button className="ver-btn" id={item.id}  onClick={editEmergencyDone}>Save</button>&nbsp;&nbsp;<button onClick={deleteEmerRow} id={item.id} className="ver-btn">Delete</button></td>
            </tr>
        )
    })

    const EmpUI=empHistory.map(empItem=>{
        return(
            <tr id={"empRow"+empItem.id}>
            <td ><span id={"empRowValclient"+empItem.id}>{empItem.client==""?<span className="text-danger">Enter client name</span>:empItem.client}</span><br/><span id={"empRowValaddress"+empItem.id}>{empItem.clientaddress==""?<span  className="text-danger">Enter client address</span>:empItem.clientaddress}</span><br/><span  id={"empRowValWorkingemail"+empItem.id}>{empItem.yourworkingmailid==""?<span  className="text-danger">Enter working mail id</span>:empItem.yourworkingmailid}</span></td>
            <td><span id={"empRowValVendorName"+empItem.id}>{empItem.vendorname==""?<span className="text-danger">Enter vendor name</span>:empItem.vendorname}</span><br/><span id={"empRowValVendorPhone"+empItem.id}>{empItem.vendorphone==""?<span  className="text-danger">Enter vendor phone</span>:empItem.vendorphone}</span><br/><span id={"empRowValVendorEmail"+empItem.id}>{empItem.vendoremail==""?<span  className="text-danger">Enter vendor email</span>:empItem.vendoremail}</span></td>
            <td><span id={"empRowValFrom"+empItem.id}>{empItem.from==""?<span  className="text-danger">from date</span>:empItem.from}</span></td>
            <td><span id={"empRowValTo"+empItem.id}>{empItem.to==""?<span  className="text-danger">to date</span>:empItem.to}</span></td>
            <td id={"empRowConsole"+empItem.id}><button className="ver-btn" id={empItem.id} onClick={editEmpFun}>Edit</button>&nbsp;&nbsp;<button className="ver-btn" id={empItem.id}  onClick={editEmpDone}>Save</button>&nbsp;&nbsp;<button onClick={deleteEmpRow} id={empItem.id} className="ver-btn">Delete</button></td>
            </tr>
        )
    })

    const WorkUI=workAuth.map(workItem=>{
        return(
            <tr id={"workRow"+workItem.id}>
            <td><span id={"empRowValType"+workItem.id}>{workItem.work_type==""?<span className="text-danger">Type</span>:workItem.work_type}</span></td>
            <td><span id={"empRowValNumber"+workItem.id}>{workItem.work_number==""?<span className="text-danger">Number</span>:workItem.work_number}</span></td>
            <td></td>
            <td><span id={"empRowValIssue"+workItem.id}>{workItem.work_issue==""?<span className="text-danger">Issue date</span>:workItem.work_issue}</span></td>
            <td><span id={"empRowValExpiry"+workItem.id}>{workItem.work_exp==""?<span className="text-danger">Expiry date</span>:workItem.work_exp}</span></td>
            <td id={"empRowConsole"+workItem.id}><button className="ver-btn" id={workItem.id} onClick={editWorkAuth}>Edit</button>&nbsp;&nbsp;<button className="ver-btn" id={workItem.id}  onClick={editWorkAuthDone}>Save</button>&nbsp;&nbsp;<button onClick={deleteWorkAuth} id={workItem.id} className="ver-btn">Delete</button></td>
            </tr>
        )
    })
    return (
        <div>
            <div className="row m-2 rounded shadow mt-4">
                <div className="user-glance col-lg-3 mt-3 mb-4">
                    <div className="user-image">
                        <img src={imageURL} alt="employeeimage" width="180" height="180"/>
                    </div>
                    <div className="user-glance-details">
                        <span><b>Employee ID: </b>{veridicId}</span><br/><br/>
                        <span><b>Name: </b>{personal.firstname+" "+personal.middlename}</span><br/>
                        <span><b>Mail ID: </b>{email}</span><br/>                        
                        <span><b>Phone: </b>{personal.phonenumber}</span><br/>
                        <span><b>Address: </b>{mailingaddress.line1+" "+mailingaddress.line2+","+mailingaddress.city+","+mailingaddress.state+","+mailingaddress.country+","+mailingaddress.zip}</span><br/>
                    </div>
                    {/* Set any one , either user or manager */}
                    {!individual?<div>
                    {adminRole?(
                         <div className="user-actions mt-3">
                        {Role=="User"? <button className="btn btn-info w-50" onClick={promote}>{loadButton?<Spinner size="sm"/>:("Promote as Manager")}</button>:<button className="btn btn-info w-50" onClick={demote}>{loadButton?<Spinner size="sm"/>:("Demote as User")}</button>}
                          </div>
                    ):null}
                    {!userRole?
                    <div className="mt-3">
                    {status=="Suspended"?<button className="btn btn-danger w-50" onClick={()=>EnableUser(email)}>{loadEnableBtn?<Spinner size="sm"/>:("Enable this User")}</button>:<button className="btn btn-danger w-50" onClick={()=>SuspendUser(email)}>{loadEnableBtn?<Spinner size="sm"/>:("Suspend this User")}</button>}
                   </div>
                   :null    
                   }
                    </div>
                     :null}
                </div>
                <div className="user-data col-lg-9 mt-4">
                    <div className="row">
                        <span className="col-4"><b>Branch:</b>{personal.branch}</span>
                        <span className="col-4"><b>Department:</b>{personal.department}</span>
                        <span className="col-4"><b>Employee Status:</b>{userstatus}</span>
                        <span className="col-4"><b>Job title:</b>{personal.jobtitle}</span>
                        <span className="col-4"><b>Reporting Manager(Supervisor):</b>{personal.reportingmanager}</span>
                    </div>
                    <div className="user-emergencycontact mt-3">
                        <h3 className=""><u>Emergency Contact:</u></h3>
                            <Table bordered>
                                <thead>
                                    <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email Id</th>
                                    <th><button className="ver-btn" onClick={addEmergency}>Add</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {EmerUI}
                                    {/* {editEmergency?
                                    EditEmerUI
                                    :
                                    EmerUI
                                    } */}
                                    {/* <tr>
                                    <td>{emergencycontact.name}</td>
                                    <td>{emergencycontact.phone}</td>
                                    <td>{emergencycontact.emailid}</td>
                                    <td><button className="ver-btn" onClick={editEmergencyFun}>Edit</button>&nbsp;&nbsp;<button className="ver-btn">Delete</button></td>
                                    </tr> */}
                                </tbody>
                            </Table>
                    </div>
                    <div className="user-employementhistory mt-3">
                    <h3><u>Employement History:</u></h3>
                            <Table bordered>
                                <thead>
                                    <tr>
                                    <th>Client Name <br/>Address<br/>Working Client Email</th>
                                    <th>Vendor Name<br/>Vendor Phone<br/>Vendor Email</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th><button className="ver-btn" onClick={addEmpHistory}>Add</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {/* <tr>
                                    <td>{employementhistory.client}<br/>{employementhistory.clientaddress}<br/>{employementhistory.yourworkingmailid}</td>
                                    <td>{employementhistory.vendorname}<br/> {employementhistory.vendorphone }<br/>{employementhistory.vendoremail}</td>
                                    <td>{employementhistory.from}</td>
                                    <td>{employementhistory.to}</td>    
                                    <td><a href="#">Edit</a>&nbsp;&nbsp;<a href="#">Delete</a></td>
                                    </tr> */}
                                    {EmpUI}
                                </tbody>
                            </Table>
                    </div>
                    <div className="user-workauth mt-3">
                    <h3><u>Work Authorization:</u></h3>
                            <Table bordered>
                                <thead>
                                    <tr>
                                    <th>Type</th>
                                    <th>Number</th>
                                    <th>Document</th>
                                    <th>Issue Date</th>
                                    <th>Expiry Date</th>
                                    <th><button className="ver-btn" onClick={addWorkAuth}>Add</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {/* <tr>
                                    <td>{workauth.work_type}</td>
                                    <td>{workauth.work_number}</td>
                                    <td></td>
                                    <td>{workauth.work_issue}</td>
                                    <td>{workauth.work_exp}</td>
                                    <td><a href="#">Edit</a>&nbsp;&nbsp;<a href="#">Delete</a></td>
                                    </tr> */}
                                    {WorkUI}
                                </tbody>
                            </Table>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Presentation
