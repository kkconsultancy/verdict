import React, { Component } from 'react'
import Presentation from './Presentation'
import fire from '../Firebase/firebase'
import $ from 'jquery'

class Container extends Component {
    constructor(){
        super();
        
    }
    
    state={
        userImage:"",
        userData:[],
        userInfo:[],
        InactiveCount:0,
        ActiveCount:0,
        SuspendedCount:0,
        userCheck:[],
        countList:0,
        userMails:[],
        userImages:[],
        adminRole:false,
        managerRole:false,
        selectCheck:false,
    }
    
    handleSort= (e) =>{
        let sortingtext=e.target.vale;
        this.state.userData.sort(()=>{

        })
    }
    MultiInvite=()=>{
        const mailsList=this.state.userCheck;

    }

    getMails=()=>{
        let allMails=this.state.userMails
        return allMails;
    }

    handleSelectAll= (e) =>{
        let allMails=this.getMails()
        if(!this.state.selectCheck){
            this.setState({selectCheck:true})
            this.setState({userCheck:allMails})
            console.log(this.state.userMails)
        }
        else{
            this.setState({selectCheck:false})
            this.setState({userCheck:[]})
        }
        
        
    }

    handleSearch=(e)=>{
        let value=e.target.value;   
        
    }

    handleChange =(mail)=>{
        let arr=["hello","bad","format","will"];
        console.log(arr)
       return console.log(arr.toString)
        console.log(mail)
        const popEle=this.state.userCheck;
        if(this.state.userCheck.includes(mail)){
            let i=popEle.indexOf(mail)
            popEle.splice(i,1)
            this.setState({userCheck:popEle})
            console.log(popEle)
            this.setState({countList:popEle.length})
        }
        else{
            let append=[...popEle,mail]
            this.setState({userCheck:append})
            console.log(append)
            this.setState({countList:append.length})
        }
        
    }

    showUser=()=>{

    }
    onHover=(e)=>{

        this.state.userImages.map(user=>{
            if(user.mail==e.target.id){
                this.setState({userImage:user.pic})
            }
        })
    }
    onHoverLeave = () => {
        this.setState({
            userImage:""
        })
      }
    componentDidMount=()=>{
        //registered users
        console.log(this.props)
        this.setState({adminRole:this.props.adminRole})
        this.setState({managerRole:this.props.managerRole})
        fire.firestore().collection("Users").onSnapshot(suc=>{      
            let users=suc.docs.map(doc=>doc.data())
            let countInactive=0;
            let CountActive=0;
            let CountSuspended=0;
            let userData=users.map(user=>{
                if(user.Role=="Admin") 
                return false
                let mails=this.state.userMails;
                if(user.status=="Inactive"){
                    countInactive++;
                    mails=[...mails,user.useremail]
                    this.setState({userMails:mails})
                    this.setState({InactiveCount:countInactive})
                    return {
                        name:" ",
                        mail:user.useremail,
                        phone:" ",
                        branch:" ",
                        employeeId:" ",
                        project:" ",
                        reportingmanager:" ",
                        employeestatus:" ",
                        usertype:" ",
                        jobtitle:" ",
                        employeetype:" ",
                        department:" ",
                        userstatus:user.status,
                        color:"#faad48"
                    }
                }
              
              // set undefined to spaced
                if(user.useremail==""||user.useremail==undefined)
                    user.useremail=" "
                if(user.personal.phonenumber==""||user.personal.phonenumber==undefined)
                    user.personal.phonenumber=" "
                if(user.personal.branch==""||user.personal.branch==undefined)
                    user.personal.branch=" "
                if(user.veridicID==""||user.veridicID==undefined)
                    user.veridicID=" "
                if(user.project==""||user.project==undefined)
                    user.project=" "
                if(user.personal.reportingmanager==""||user.personal.reportingmanager==undefined)
                    user.personal.reportingmanager=" "
                if(user.employeestatus==""||user.employeestatus==undefined)
                    user.employeestatus=" "
                if(user.Role==""||user.Role==undefined)
                    user.Role=" "
                if(user.personal.jobtitle==""||user.personal.jobtitle==undefined)
                    user.personal.jobtitle=" "
                if(user.workauth.work_type==""||user.workauth.work_type==undefined)
                    user.workauth.work_type=" "
                if(user.personal.department==""||user.personal.department==undefined)
                    user.personal.department=" "
                if(user.status==""||user.status==undefined)
                    user.status=" "
                      
                // providing data for manager only
                if(this.props.managerRole){
                  // counting based on status
                    if(user.Role=="Manager"){
                        return false
                    }    
                    mails=[...mails,user.useremail]
                    this.setState({userMails:mails})
                    if(user.status=="Active"){
                        CountActive++;
                        this.setState({ActiveCount:CountActive});
                        let userImages=this.state.userImages;
                        let image={
                            mail:user.useremail,
                            pic:user.imageURL
                        }
                        userImages.push(image);
                        
                    }
                    if(user.status=="Suspended"){
                        CountSuspended++
                        this.setState({SuspendedCount:CountSuspended})
                        let userImages=this.state.userImages
                        let image={
                            mail:user.useremail,
                            pic:user.imageURL
                        }
                        userImages.push(image)
                    }
                    if(user.Role=="User"){
                        return {
                            name:user.personal.firstname+" "+user.personal.middlename,
                            mail:user.useremail,
                            phone:user.personal.phonenumber,
                            branch:user.personal.branch,
                            employeeId:user.veridicID,
                            project:user.project,
                            reportingmanager:user.personal.reportingmanager,
                            employeestatus:user.employeestatus,
                            usertype:user.Role,
                            jobtitle:user.personal.jobtitle,
                            employeetype:user.workauth.work_type,
                            department:user.personal.department,
                            userstatus:user.status,
                            image:user.imageURL,
                            color:"#f58369"
                        }
                    }
                }

                // providing data for admin only
                if(this.props.adminRole){
                    mails=[...mails,user.useremail]
                    this.setState({userMails:mails})
                    if(user.status=="Active"){
                        CountActive++;
                        this.setState({ActiveCount:CountActive});
                        let userImages=this.state.userImages;
                        let image={
                            mail:user.useremail,
                            pic:user.imageURL
                        }
                        userImages.push(image);
                        
                    }
                    if(user.status=="Suspended"){
                        CountSuspended++
                        this.setState({SuspendedCount:CountSuspended})
                        let userImages=this.state.userImages
                        let image={
                            mail:user.useremail,
                            pic:user.imageURL
                        }
                        userImages.push(image)
                    }
                    return {
                        name:user.personal.firstname+" "+user.personal.middlename,
                        mail:user.useremail,
                        phone:user.personal.phonenumber,
                        branch:user.personal.branch,
                        employeeId:user.veridicID,
                        project:user.project,
                        reportingmanager:user.personal.reportingmanager,
                        employeestatus:user.employeestatus,
                        usertype:user.Role,
                        jobtitle:user.personal.jobtitle,
                        employeetype:user.workauth.work_type,
                        department:user.personal.department,
                        userstatus:user.status,
                        image:user.imageURL,
                        color:"#f58369"
                    }
                }
            })
            
            this.setState({userData:userData})
              
        })
    }
    
    render() {
        return (
            <div>
                <Presentation 
                onHover={this.onHover} 
                onHoverLeave={this.onHoverLeave} 
                handleSelectAll={this.handleSelectAll}  
                handleChange={this.handleChange} 
                {...this.state} 
                userData={this.state.userData}
                handleSearch={this.handleSearch}
                />
            </div>
        )
    }
}

export default Container
