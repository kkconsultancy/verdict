import React, { useState }  from 'react'
import { Button,Row,Col,Table,CustomInput,Badge,Popover,PopoverBody,PopoverHeader,Input,Spinner } from 'reactstrap';
import {Link} from 'react-router-dom'
import {Label} from 'semantic-ui-react'
import { MDBDataTable } from 'mdbreact';
import $ from 'jquery'
function Presentation(props) {
    // const [popoverOpen, setPopoverOpen] = useState(false);
    // const toggle = () => setPopoverOpen(!popoverOpen);
    const {handleSearch,found,adminRole,managerRole,userImage,filterBy,userMails,showUser,onHover,popoverOpen,onHoverLeave,imageURL,handleChange,handleSort,handleSelectAll,countList,userCheck,userData,InactiveCount,SuspendedCount,ActiveCount}=props
  
    let rowData=userData.map(user=>{
        return{
            name:user.name,
            mail:user.mail,
            phone:user.phone,
            branch:user.branch,
            employeeId:user.employeeId,
            project:user.project,
            reportingmanager:user.reportingmanager,
            employeestatus:user.employeestatus,
            usertype:user.usertype,
            jobtitle:user.jobtitle,
            employeetype:user.jobtitle,
            department:user.department,
            userstatus:user.userstatus,
        }
       
    })

    const data={
        columns:[
            {
                label: 'Name',
                field: 'name',
                sort: 'asc',
                width: 150
              },
              {
                label: 'Mail ID',
                field: 'mail',
                sort: 'asc',
                width: 270
              },
              {
                label: 'Phone',
                field: 'phone',
                sort: 'asc',
                width: 200
              },
              {
                label: 'Branch',
                field: 'branch',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Employee ID',
                field: 'employeeId',
                sort: 'asc',
                width: 150
              },
              {
                label: 'Project',
                field: 'project',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Reporting Manager',
                field: 'reportingmanager',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Employee Status',
                field: 'employeestatus',
                sort: 'asc',
                width: 100
              },
              {
                label: 'User Type',
                field: 'usertype',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Job Title',
                field: 'jobtitle',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Employee Type',
                field: 'employeetype',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Department',
                field: 'department',
                sort: 'asc',
                width: 100
              },
              {
                label: 'User Status',
                field: 'userstatus',
                sort: 'asc',
                width: 100
              }
        ],

        rows:rowData
    
    }
   
    let checkBox=[...userCheck];
    const placeUsers=userData.map(user=>{
        if(!user) 
        return false
        if(adminRole){
        return(
                 
                 <tbody style={user.userstatus=="Suspended"||user.userstatus=="Inactive"?{backgroundColor:user.color}:{backgroundColor:"white"}}>
                        <tr id="EmployeeRow">
                        <td><span id="emplist_name">{user.name}</span></td>
                        <td><span id="emplist_mail">{user.mail}</span></td>
                        <td><span id="emplist_phone">{user.phone}</span></td>
                        <td><span id="emplist_branch">{user.branch}</span></td>
                        <td><span id="emplist_id"><Link onMouseEnter={onHover}  onMouseLeave={onHoverLeave} id={user.mail} to={"/dashboard/console/employeelist/"+user.mail}><h5>{user.employeeId}</h5></Link></span></td>
                        <td><span id="emplist_project">{user.project}</span></td>
                        <td><span id="emplist_reportingmanager">{user.reportingmanager}</span></td>
                        <td><span id="emplist_status">{user.employeestatus}</span></td>
                        <td><span id="emplist_usertype">{user.usertype=="Manager"?<Label color='blue'>{user.usertype}</Label>:<React.Fragment>{user.usertype}</React.Fragment>}</span></td>
                        <td><span id="emplist_jobtitle">{user.jobtitle}</span></td>
                        <td><span id="emplist_employeetype">{user.employeetype}</span></td>
                        <td><span id="emplist_department">{user.department}</span></td>
                        <td><span id="emplist_userstatus">{user.userstatus}</span></td>
                        <td><CustomInput className="selectCheckBox" type="checkbox" id={"email:"+user.mail} onChange={()=>handleChange(user.mail)} label="" checked={checkBox.includes(user.mail)}/></td>
                        </tr>
                    </tbody>
                   
                  
        )
      }
      if(managerRole){
          if(user.usertype!="Manager"){
            return(
                 
              <tbody className="" style={user.userstatus=="Suspended"||user.userstatus=="Inactive"?{backgroundColor:user.color}:{backgroundColor:"white"}}>
                    <tr id="EmployeeRow">
                    <td><span id="emplist_name">{user.name}</span></td>
                    <td><span id="emplist_mail">{user.mail}</span></td>
                    <td><span id="emplist_phone">{user.phone}</span></td>
                    <td><span id="emplist_branch">{user.branch}</span></td>
                    <td><span id="emplist_id"><Link onMouseEnter={onHover}  onMouseLeave={onHoverLeave} id={user.mail} to={"/dashboard/console/employeelist/"+user.mail}>{user.employeeId}</Link></span></td>
                    <td><span id="emplist_project">{user.project}</span></td>
                    <td><span id="emplist_reportingmanager">{user.reportingmanager}</span></td>
                    <td><span id="emplist_status">{user.employeestatus}</span></td>
                    <td><span id="emplist_usertype">{user.usertype=="Manager"?<Badge>{user.usertype}</Badge>:<React.Fragment>{user.usertype}</React.Fragment>}</span></td>
                    <td><span id="emplist_jobtitle">{user.jobtitle}</span></td>
                    <td><span id="emplist_employeetype">{user.employeetype}</span></td>
                    <td><span id="emplist_department">{user.department}</span></td>
                    <td><span id="emplist_userstatus">{user.userstatus}</span></td>
                    <td><CustomInput className="selectCheckBox" type="checkbox" id={"mail_"+user.mail} onChange={()=>handleChange(user.mail)} label="" checked={checkBox.includes(user.mail)}/></td>
                    </tr>
                </tbody>
    )
          }
      }
    })
    
    

   
    return (
        <div>
            <div className="employeebox">   
                
                <div className="employeenav m-1">
                   <div className="row">
                       <div className="emp_glance col-4 mb-4" style={{height:"100px"}}>
                           {userImage?<img src={userImage} height="100"  className="border"/>:<p>No Image</p>}
                            
                       </div>
                       <div className="emp_actions col-8">
                            <Row>
                                <Col><button className="ver-btn">Hold Timesheets</button></Col>
                                <Col><button className="ver-btn">Send Announcement</button></Col>
                                <Col><button className="ver-btn">Send Text Message</button></Col>
                                <Col><button className="ver-btn">Send Email</button></Col>
                                <Col><button className="ver-btn">Invite user</button></Col>
                            </Row>
                       </div>
                   </div>
                   <div className="d-flex justify-content-between">
                        <div className=" mt-3">
                            <span className="ver-label col-sm">Active:<Badge color="success">{ActiveCount}</Badge></span>
                            <span className="ver-label col-sm">In Active:<Badge color="warning">{InactiveCount}</Badge></span>
                            <span className="ver-label col-sm">Suspended:<Badge color="danger">{SuspendedCount}</Badge></span>
                            <span className="ver-label col-sm">Selected:<Badge color="primary">{checkBox.length}</Badge></span>
                            
                        </div>
                        <div>
                          <span><Input type="text" placeholder="Search" onChange={handleChange}/></span>
                        </div>
                   </div>
                
                </div>
                
                <div id="popHere" className="tableFixHead employeelist w-100" style={{overflowX:"auto",height:"500px"}}>
                    <table bordered className="rounded table table-fixed">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Mail ID</th>
                        <th>Phone</th>
                        <th>Branch</th>
                        <th>Employee ID</th>
                        <th>Project</th>
                        <th>Reporting Manager</th>
                        <th>Employee Status</th>
                        <th>User type</th>
                        <th>Job Title</th>
                        <th>Employee Type</th>
                        <th>Department</th>
                        <th>User Status</th>
                        <th><CustomInput type="checkbox" id="selectall" onChange={handleSelectAll} label="Select all"/></th>
                        </tr>
                    </thead>
                    <React.Fragment>
                    {placeUsers?placeUsers:<Spinner/>}
                    </React.Fragment>
                    
                    </table>
                </div>
              
            </div>
        </div>
    )
}

export default Presentation
