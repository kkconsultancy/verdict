import React, { Component } from 'react'
import Presentation from './Presentation'
import Axios from 'axios'
import fire from '../../Firebase/firebase'
class Container extends Component {
    state={
        projectList:[],
        fetchMails:true,
        options:[],
        userMails:[]
    }
    
    componentDidMount=()=>{
        const { adminRole,email } = this.props
       fire.firestore().collection("Projects")
       .onSnapshot(data=>{
           let projects=[];
           if(!adminRole){
               let projectIdList=[]
            data.forEach(project=>{
                for(let user in project.data().Users){
                    if(user==email){
                       if(projects.indexOf(project.id)==-1){
                        projectIdList.push(project.id)
                       }
                    }
                }
            })
            data.forEach(project=>{
                projectIdList.map(projectList=>{
                    if(projectList==project.id){
                        projects.push({
                            id:project.id,
                            clientname:project.data().title,
                            supervisor:project.data().supervisor,
                            startdate:project.data().startdate,
                            enddate:project.data().enddate,
                            status:project.data().status
                        })
                    }
                })
            })
           }
           else{
            data.forEach(project=>{
                projects.push({
                    id:project.id,
                    clientname:project.data().title,
                    supervisor:project.data().supervisor,
                    startdate:project.data().startdate,
                    enddate:project.data().enddate,
                    status:project.data().status
                })
            })
           }
           
           this.setState({projectList:projects})
       })

       Axios.get(`http://localhost:5001/flair-d7b59/us-central1/api/activeusers`)
       .then(data=>{   
           let mailData=data.data
        let userMails= mailData.map(mail=>{
               return {
                   key:mail,
                   text:mail,
                   value:mail
               }
          }) 
         this.setState({userMails})
         this.setState({
           options:userMails,
           fetchMails:false
         })
       })
       .catch(err=>{
           console.log(err)
       })
    }

    handleProject=(e)=>{
        
    }
    render() {
        return (
            <div>
                <Presentation
                {...this.state}
                userRole={this.props.userRole}
                email={this.props.email}
                />
            </div>
        )
    }
}

export default Container
