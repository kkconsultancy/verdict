import React from 'react'
import ReactTable from 'react-table-6'
import {CustomInput,Badge} from 'reactstrap'
import {Link} from 'react-router-dom'
import {Button} from 'semantic-ui-react'
import NewProject from './NewProject'
import "react-table-6/react-table.css"
import ExportToExcel from './ExportToExcel'
import $ from 'jquery'
import Pdf from './Pdf'



class Presentation extends React.Component{
   

    handleExcel=()=>{
      $("#test-table-xls-button").trigger("click");
    }
    handlePdf=()=>{
      console.log("got")
      $("#export_to_pdf_btn").trigger("click");
    }
  

       render(){
        const {projectList, fetchMails, options, userMails, userRole, email}=this.props
        console.log(projectList)
        const  columns=[
                {
                    Header:<CustomInput type="checkbox" id="checkb"/>,
                    Cell:props=>{
                        return (
                            <CustomInput type="checkbox" id="checkb"/> 
                        )
                    },
                    sortable:false,
                    style:{
                        textAlign:"center"
                    },
                    filterable:false
                },
                 {
                    Header: 'Client Name',
                    id:"clientname",
                    accessor: projectList=><Link  to={"/dashboard/console/projectmanagement/"+projectList.id}>{projectList.clientname}</Link>,
    
                    filterMethod: (filter, projectList) => {
                      console.log(filter.value)
                      console.log(projectList._original.clientname)
                      return !projectList._original.clientname.indexOf(filter.value);
                      //return projectList.indexOf(item => item.indexOf(filter.clientname) >= 0) >= 0;
                    }
                  },
                  {
                    Header: 'Supervisor',
                    accessor: 'supervisor',
                  },
                  {
                    Header: 'Start date',
                    accessor: 'startdate',
                    sortable:false,
                    filterable:false
                  },
                  {
                    Header: 'End date',
                    accessor: 'enddate',
                    sortable:false,
                    filterable:false
                  },
                  {
                    Header: 'Status',
                    accessor: 'status',
                  }
                ]
     
    return (
        <div> 
           
            <div className="projectconsole d-flex justify-content-between bg-light m-1">
                <div className="projectcount m-2 d-flex">
                    <span><h5>Projects:<i className="bg-dark p-1 text-light rounded">3</i></h5></span>
                    <span><h5>Selected:</h5></span>
                </div>      
                <div className="newproject m-2 d-flex">
                    <span><button onClick={this.handlePdf}>PDF</button></span>
                    <span><button  onClick={this.handleExcel}>EXCEL</button></span>
                    {!userRole?<span>
                         <NewProject 
                         fetchMails={fetchMails}
                         options={options}
                         email={email}
                         userMails={userMails}/>
                    </span>:null}
                </div>
            </div>
            <ReactTable
            id="rtable"
            columns={columns}
            data={projectList}
            filterable
            defaultPageSize={10}
            
            >
            {(state,filteredData, instance) =>{
               const reactTable=state.pageRows.map(post => {
                 console.log(post._original)
                 return post._original
                });
              return(
                <div>
                  {filteredData()}
                <ExportToExcel posts={reactTable}/>
                <Pdf pdfdata={reactTable}/>
                </div>

              )
            }}
          </ReactTable>
            
        </div>
    )
  }
  }
export default Presentation