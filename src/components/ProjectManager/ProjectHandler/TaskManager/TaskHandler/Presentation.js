import React from 'react'
import { Segment, Grid } from 'semantic-ui-react'
function Presentation(props) {
    const { taskInfo } = props
    console.log(taskInfo)
    return (
        <div>
            <Segment className="m-4 p-4">
                <Grid columns="2">
                    <Grid.Row>
                        <Grid.Column>
                            <span>Created By:</span>
                        </Grid.Column>
                        <Grid.Column>
                            <span>Due By:</span>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <span>Assigned to:{taskInfo.assignee}</span>
                        </Grid.Column>
                        <Grid.Column>
                            <span>Priority:{taskInfo.priority}</span>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <span>Progress:</span>
                        </Grid.Column>
                        <Grid.Column>
                            {/* <span>Priority:{taskInfo.priority}</span> */}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        </div>
    )
}

export default Presentation
