import React, { Component } from 'react'
import Presentation from './Presentation'
import axios from 'axios'
 class Container extends Component {
    state={
        taskInfo:[]
    }

    componentDidMount=()=>
    {
        console.log(this.props)
        const {projectId,taskId} = this.props.match.params
        if(projectId&&taskId){
        axios.get(`http://localhost:5001/flair-d7b59/us-central1/api/projects/${projectId}/tasks/${taskId}`)
        .then(data=>{
            console.log(data)
            this.setState({taskInfo:data.data})
        })
        .catch(err=>{
            console.log(err)
        })
        }
    }
    render() {
        return (
            <div>
                <Presentation
                {...this.state}
                />
            </div>
        )
    }
}

export default Container
