import React from "react";
import { MDBDataTable } from "mdbreact";
import {CustomInput} from 'reactstrap';
import $ from 'jquery'



function Settings(props) {
  console.log(props.usersAccess)
  const { usersAccess } = props
  let placeObj={}
  let access=[]
  $.each(usersAccess,(i,v)=>{
    access.push({
      name:i,
      create:v.create,
      update:v.update,
      read:v.read,
      delete:v.delete
    })
  })
  console.log(access)
  const data = {
    
    columns: [
   {   
        label: "FullName",
        field: "FullName",
        sort: "asc",
      },
      {
        label: "Create",
        field: "Create",
        sort: "asc"},
      {
        label: "Read",
        field: "Read",
        sort: "asc"

      },
      {
        label: "Update",
        field: "Update",
        sort: "asc"
      },
      {
        label:"delete",
        field: "delete",
        sort: "asc"
      }     
    ],
    rows: access.map(user=>{
      return{
        FullName: user.name,
        Create: <CustomInput label="" type="checkbox" id="2" checked={user.create}/>,
        Read: <CustomInput label="" type="checkbox" id="3" checked={user.read}/>,
        Update: <CustomInput label="" type="checkbox" id="4" checked={user.update}/>,
        delete: <CustomInput label="" type="checkbox" id="5" checked={user.delete}/>,
      }
    })
  };

  return(
   <div>

  <center><h1>Project member settings</h1></center>
<MDBDataTable 
          striped 
          bordered 
           data={data} 
           />
</div>
  ) 
};

export default Settings;
