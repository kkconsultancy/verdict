import React, { Component } from 'react'
import Presentation from './Presentation'
import axios from 'axios'
import fire from '../../Firebase/firebase'
import {List} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import {Spinner} from 'reactstrap'
class Container extends Component {
    state={
        project:{},
        activeIndex1:0,
        usersAccess:[],
        placeUsers:[],
        userList:[],
        projectId:"",
        loading:true
    }
    componentDidMount=()=>{
        console.log(this.props)
        let projectId=this.props.match.params.projectId
        this.setState({projectId})
        fire.firestore().collection("Projects")
        .doc(projectId)
        .onSnapshot(project=>{  
            let data=project.data()
            this.setState({project:data})
            this.setState({usersAccess:data.Users})
            
        this.setState({loading:false})
            console.log(data)
            let users=[]
            for(let user in data.Users){
                users.push(user)
            }
            this.setState({userList:users})
            let placeUsers=users.map(user=>{    
                    return <Link to={`/dashboard/console/employeelist/${user}`}><List>{user}</List></Link>
             })
                    this.setState({placeUsers})
            this.setState({placeUsers})
        })
    
        // axios.get(`https://us-central1-flair-d7b59.cloudfunctions.net/api/projects/${this.props.match.params.projectId}`)
        // .then(project=>{    
        //     this.setState({project:project.data})
        //     this.setState({currUsers:project.data.Users})
        //     let placeUsers=project.data.Users.map(user=>{
        //     return <Link to={`/dashboard/console/employeelist/${user}`}><List>{user}</List></Link>
        //     })
        //     this.setState({placeUsers})
        // })
        // .catch(err=>{
        //     console.log(err)
        // })

        

      

    }

    handleClick1 = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex1 } = this.state
        const newIndex = activeIndex1 === index ? -1 : index
    
        this.setState({ activeIndex1: newIndex })
      }
      
    addMember=()=>{

    }  
    render() {
        return (
            <div>
               {
                   this.state.loading?<Spinner className="ml-auto"/>:(
                <Presentation
                {...this.state}
                handleClick1={this.handleClick1}
                addMember={this.addMember}
                />
                   )
                }
            </div>
        )
    }
}

export default Container
