const functions = require('firebase-functions');
const nodemailer=require("nodemailer")
const jsonBodyParser=require("body-parser").json()
const express=require('express')
const app=express();
const cors = (require('cors'))({origin:true})
// Authentication related
app.use(cors)
const { login } = require('./handlers/Auth')
// Project related
const { getProjectsData,createNewProject,singleProjectData } = require('./handlers/Project')
// Task related
const { createNewTask,getTasks,getTask } = require('./handlers/Task')
// User related
const { getUserMails,getActiveUserMails,getUsers,getUserUID,getUser } = require('./handlers/Users')
// History
const { getHistory } = require("./handlers/History")

// Project Related api
app.post("/project", createNewProject);
app.get("/projects",getProjectsData)
app.get("/projects/:projectId",singleProjectData)

// Task related api
app.post("/task",createNewTask)
app.get("/projects/:projectId/tasks",getTasks)
app.get("/projects/:projectId/tasks/:taskId",getTask)
// Users related api
app.get("/users",getUserMails)
app.get("/activeusers",getActiveUserMails)
app.get("/usersdata",getUsers)
app.post("/user",getUser)
app.post("/useruid",getUserUID)
// History
app.get("/history",getHistory)



// invite new user
exports.newUserEmail = functions.https.onRequest((request,response) => cors(request,response,() => {
    const email = request.body.useremail
    console.log(email)
    return admin.auth().createUser({
        uid : email,
        email : email,
        password : "veridic0004"
    }).then(res => {
        return admin.auth().createCustomToken(email,{
            isRegister : true,
            timestamp : request.body.time
        })
    }).then(res => {
        return response.send({
            token : res
        })
    })
    .catch(err => {
        response.send(err)
    })
    
}))


exports.api=functions.https.onRequest(app)