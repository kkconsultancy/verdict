
exports.login=(req,res)=>{
    let email=req.body.email;
    let pass=req.body.password;
    firebase.auth().signInWithEmailAndPassword(email,pass)
    .then(token=>{
       return res.json(token)
    })
    .catch(err=>{
        return res.json(err)
    })
}