const {admin} = require('../utils/admin')
const cors= (require("cors"))({origin:true});
const functions = require("firebase-functions")
const { History } = require("./History")


exports.addUserRole=functions.https.onCall((data,context)=>{
    let historySnap={
        subject:data.email,
        ActionBy:data.ActionBy,
        createdAt: admin.firestore.Timestamp.fromDate(new Date()),
        type:"demote"
    }
    return admin.auth().getUserByEmail(data.email).then(user=>{
        return admin.auth().setCustomUserClaims(user.uid,{
            user:true
        })
    }).then((suc)=>{
        History(historySnap);
        return res.send({
            message:`Success! ${data.email} has been made as user`,
            success:true
        })
    }).catch(err=>{
        return res.send({
            error:err,
            success:false
        })
    })
})

exports.addManagerRole=functions.https.onCall((data,context)=>{
    let historySnap={
        subject:data.email,
        ActionBy:data.ActionBy,
        createdAt: admin.firestore.Timestamp.fromDate(new Date()),
        type:"promotion"
    }
    return admin.auth().getUserByEmail(data.email).then(user=>{
        return admin.auth().setCustomUserClaims(user.uid,{
            manager:true
        })
    }).then(()=>{
        History(historySnap);
        return {
            message:`Success! ${data.email} has been made as manager`,
            success:true
        }
    }).catch(err=>{
        return {
            error:err,
            success:false
        }
    })
})

exports.addAdminRole=functions.https.onCall((data,context)=>{
    return admin.auth().getUserByEmail(data.email).then(user=>{
        return admin.auth().setCustomUserClaims(user.uid,{
            admin:true
            
        })
    }).then(()=>{
        return {
            message:`Success! ${data.email} has been made as admin`,
            success:true
        }
    }).catch(err=>{
        return {
            error:err,
            success:false
        }
    })
})

exports.disableUser=functions.https.onRequest((data,context)=>{ 
    let historySnap={
        subject:data.email,
        ActionBy:data.ActionBy,
        createdAt: admin.firestore.Timestamp.fromDate(new Date()),
        type:"suspenduser"
    }
    return admin.auth().updateUser(data.uid,{
        disabled:true
    }).then((suc)=>{
        History(historySnap);
        console.log(suc)
        return {message:`User ${data.uid} disabled`,
                success:true}
    }).catch(err=>{
        History(historySnap);
        return {
            error:err,
            success:false
        }
    })
})

//enable user
exports.enableUser=functions.https.onCall((data,context)=>{
    let historySnap={
        subject:data.email,
        ActionBy:data.ActionBy,
        createdAt: admin.firestore.Timestamp.fromDate(new Date()),
        type:"enableuser"
    }
    return admin.auth().updateUser(data.uid,{
        disabled:false
    }).then(()=>{
        History(historySnap);
        return {message:`User ${data.uid} enabled`,
            success:true
        }
    }).catch(err=>{
        return {
            error:err,
            success:false
        }
    })
})

// verify ID token

exports.verifyID = functions.https.onRequest((req,res)=>cors(req,res,()=>{
    console.log(req.body.id)
    admin.auth().verifyIdToken(req.body.id)
    .then((data)=>{
        console.log(data)
       return res.send(data)
    })
    .catch(err=>{
        console.log(err)
        return err;
    })
}))




//node mailer

exports.sendEmail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'sunnychow90@gmail.com',
              pass: '9182770300'
            } ,
            tls: {
              rejectUnauthorized: false
          },
          });
        // getting dest email 
        const dest = req.body.email;
        const pass=req.body.pass;
        const mailOptions = {
            from: 'sunnychow90@gmail.com', // Something like: Jane Doe <janedoe@gmail.com>
            to: dest,
            subject: 'Invitation to register your profile in veridic solutions',
            html: `
                    <p>https://flair-d7b59.firebaseapp.com/dashboard/console/postform/${dest}</p>
                    <br/>
                    <p>Password:${pass}</p>`
        };
        let historySnap={
            subject:dest,
            ActionBy:req.body.ActionBy,
            createdAt: admin.firestore.Timestamp.fromDate(new Date()),
            type:"inviteuser"
        }
        // returning result
        return transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              return res.send(error);
            } else {
                History(historySnap);
                return res.send('Email sent: ' + info.response);
            }
          });
    });    
});

// send multiple mails
exports.sendEmail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'sunnychow90@gmail.com',
              pass: '9182770300'
            } ,
            tls: {
              rejectUnauthorized: false
          },
          });
        // getting dest email 
        var maillist=req.body.maillist;
        const dest = req.body.email;
        const pass=req.body.pass;

        const mailOptions = {
            from: 'sunnychow90@gmail.com', // Something like: Jane Doe <janedoe@gmail.com>
            to: dest,
            subject: 'Invitation to register your profile in veridic solutions',
            html: `
                    <p>https://flair-d7b59.firebaseapp.com/dashboard/console/postform/${dest}</p>
                    <br/>
                    <p>Password:${pass}</p>`
        };
  
        // returning result
        return transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              return res.send(error);
            } else {
                return res.send('Email sent: ' + info.response);
            }
          });
    });    
});

    