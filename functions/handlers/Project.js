const cors= (require("cors"))({origin:true});
const {db, admin} = require('../utils/admin')
const { History } = require("./History")
// get entire project data
exports.getProjectsData=(req,res)=>cors(req,res,()=>{
    db.collection("Projects")
    .onSnapshot(data=>{
        let projects=[];
        data.forEach(project=>{
            projects.push({ 
                id:project.id,
                clientname:project.data().title,
                supervisor:project.data().supervisor,
                startdate:project.data().startdate,
                enddate:project.data().enddate,
                status:project.data().status
            })
        })
        return res.json(projects)
    })
})

// create new project
exports.createNewProject= (req,res) =>cors(req,res,()=> {
      const newProject= {
        title: req.body.title,
        description: req.body.description,
        status: req.body.status,
        startdate: req.body.startdate,
        enddate: req.body.enddate,
        supervisor: req.body.supervisor,
        Users:req.body.Users,
        createdBy:req.body.createdBy,
        createdAt: admin.firestore.Timestamp.fromDate(new Date())
      }
      const historySnap={
        subject:newProject.title,
        ActionBy:newProject.createdBy,
        createdAt:newProject.createdAt,
        type:"createProject"
      }
      db.collection("Projects")
        .add(newProject)
        .then(doc => {
          History(historySnap);
          return res.send({message:`Project ${doc.id} created successfully`})
        })
        .catch(err => {
          res.status(500).json({error:`Something went wrong, ${err}`})
        });
    
  })

  // get single project data

  exports.singleProjectData=(req,res)=>cors(req,res,()=>{
    const projectId=req.params.projectId
    db.collection("Projects")
    .doc(projectId)
    .onSnapshot(doc=>{
        return res.json(doc.data())
    })
})


