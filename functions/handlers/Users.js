const cors=require("cors")({origin:true});
const {db} = require('../utils/admin')
const {admin} = require('../utils/admin')

exports.getUserUID=(req,res)=>cors(req,res,()=>{
    admin.auth().getUserByEmail(req.body.email)
    .then(user=>{
        return res.json(user.uid)
    })
    .catch(err=>{
        return err;
    })
})

exports.getUserMails=(req,res)=>cors(req,res,()=>{
    db.collection("Users")
    .get()
    .then(data=>{
        let userMails=[]
        data.forEach(user=>{
            userMails.push(user.id)
        })
        return res.json(userMails)
    })
    .then(suc=>console.log(success))
    .catch(err=>res.send(err))
})

exports.getActiveUserMails=(req,res)=>cors(req,res,()=>{
    db.collection("Users")
    .get()
    .then(data=>{
        let userMails=[]
        data.forEach(user=>{
            if(user.data().status==="Active"){
                userMails.push(user.id)
            }
        })
        return res.json(userMails)
    })
    .catch(err=>res.send(err))
})

exports.getUsers=(req,res)=>cors(req,res,()=>{
    db.collection("Users")
    .get()
    .then(data=>{
        let users=[]
        data.forEach(user=>{
            users.push(user.data())
        })
        return res.json(users)
    })
    .catch(err=>{
        return res.json(err);
    })
})


exports.getUser=(req,res)=>cors(req,res,()=>{
    let email=req.body.email
    db.collection("Users")
    .doc(email)
    .get()
    .then(data=>{
        return res.json(data.data())
    })
    .catch(err=>{
        return res.json(err)
    })
})  