
const cors= (require("cors"))({origin:true});
const {db} = require('../utils/admin')
exports.getHistory=(req,res)=>cors(req,res,()=>{
    db.collection("History")
    .get()
    .then(data=>{
        let history=[]
        data.forEach(item=>{
            history.push(item.data())
        })
        return res.send(history)
    })
    .catch(err=>{
        return res.send(err)
    })
})

exports.History=(result)=>{
  db.collection("History")
  .add(result)
  .then(res=>{
    console.log("History Updated")
  })
  .catch(err=>{
    console.log("Failed to update in History")
  })
}